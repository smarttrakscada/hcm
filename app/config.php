<?php 
ini_set('error_log', 'data/smarthcm.log');

define('CLIENT_NAME', 'app');
define('APP_BASE_PATH', '/var/www/hcm.smarttrakportal.com/');
define('CLIENT_BASE_PATH', '/var/www/hcm.smarttrakportal.com/app/');
define('BASE_URL','http://hcm.smarttrakportal.com/');
define('CLIENT_BASE_URL','http://hcm.smarttrakportal.com/app/');

define('APP_DB', 'smarthcm');
define('APP_USERNAME', 'hcm');
define('APP_PASSWORD', 'smarttrak');
define('APP_HOST', 'localhost');
define('APP_CON_STR', 'mysql://'.APP_USERNAME.':'.APP_PASSWORD.'@'.APP_HOST.'/'.APP_DB);

//file upload
define('FILE_TYPES', 'jpg,png,jpeg');
define('MAX_FILE_SIZE_KB', 10 * 1024);
